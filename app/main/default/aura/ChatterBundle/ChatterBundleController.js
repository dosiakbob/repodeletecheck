({
    init: function (component, event, helper) {
        helper.doinitMethod(component, event, helper);
        helper.getcurrentUserId(component);
        // helper.loadOptions(component, event, helper);

    },

    handleClick: function (component, event, helper) {
        var newComment = component.get("v.newComment");
        var action = component.get("c.save");
        if (!newComment == '') {
            action.setParams({
                "chatter": newComment
            });
            action.setCallback(this, function (response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var name = response.getReturnValue();
                    helper.doinitMethod(component, event, helper);
                }
                else if (state === "ERROR") {
                    alert("Failed");
                }
            });
            $A.enqueueAction(action);
        } else {
            alert('Post cannot be empty');
        }
    },

    handleKeyUp: function (component, event, helper) {
        var com = component.get('v.newComment');
        var key = event.keyCode;
        if (!component.get('v.startSerch') && key === 50) {
            component.set('v.startSerch', true);
        } else if (component.get('v.startSerch') && (key === 13 || key === 32)) {
            component.set('v.startSerch', false);
        } else if (component.get('v.startSerch')) {
            helper.searchUsers(component);
        }
    },

    handleChange: function (component, event) {
        var tmpUsrName = component.get("v.toUsers");
        var tmp = component.get("v.toUsers").find(label => tmpUsrName);
        var comment = component.get('v.newComment');
        var oldValue = comment.substring(comment.lastIndexOf('@') + 1, comment.indexOf('<', comment.lastIndexOf('@')));
        var newComment = comment.substring(0, comment.lastIndexOf('@')) + tmp.label + ',  ' + comment.substring(comment.lastIndexOf('@') + oldValue.length + 1, comment.length);
        component.set('v.newComment', newComment);
        component.set('v.startSerch', false);
    },
    handleUploadFinished: function (component, event, helper) {
        var uploadedFiles = event.getParam("files");
        var documentId = uploadedFiles[0].documentId;
        var fileName = uploadedFiles[0].name;
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Success!",
            "message": "File " + fileName + " Uploaded successfully."
        });
        toastEvent.fire();
        if (documentId != null) {
            component.set('v.docId', documentId)
            helper.addCommentAfterUpload(component, event, helper);
            helper.doinitMethod(component, event, helper);
        }

    },
    newEvent: function (component, event, helper) {
        var createRecordEvent = $A.get("e.force:createRecord");
        createRecordEvent.setParams({
            "entityApiName": "Event"
        });
        createRecordEvent.fire();
        helper.doinitMethod(component, event, helper);
    },

    chekboxCheck: function (component, event, helper) {
        var checked = component.get("v.ifChecked")
        if (!checked) {
            helper.showOnlyToMe(component, event, helper);
        } else if (checked) {
            console.log('NOTchecked!' + checked);
            helper.doinitMethod(component, event, helper);
        }

    },
    //////////////////////////////////////////////////////////
    cmntThis: function (component, event, helper) {
        var value = event.getSource().get("v.value");
        component.set('v.indexToEdit', value);
        var wannacmnt = component.get("v.wannaComent");
        if (!wannacmnt) {
            wannacmnt = component.set("v.wannaComent", true);
        } else {
            wannacmnt = component.set("v.wannaComent", false);
        }
    },
    shareSubComment: function (component, event, helper) {
        var wannacmnt = component.get("v.wannaComent");
        var newComment = component.get("v.newSubComment");
        var getchatterId = component.get("v.indexToEdit");
        var action = component.get("c.saveSubComment");
        if (!newComment == '') {
            action.setParams({
                "commentMessage": newComment,
                "parentId": getchatterId
            });
            action.setCallback(this, function (response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var name = response.getReturnValue();
                    helper.doinitMethod(component, event, helper);
                    wannacmnt = component.set("v.wannaComent", false);
                }
                else if (state === "ERROR") {
                    alert("Failed");
                }
            });
            $A.enqueueAction(action);
        } else {
            alert('Post cannot be empty');
        }
    },
    newCase: function (component, event, helper) {
        var createRecordCase = $A.get("e.force:createRecord");
        createRecordCase.setParams({
            "entityApiName": "Case"
        });
        createRecordCase.fire();
        helper.doinitMethod(component, event, helper);
    },
    chekboxCheckCase: function (component, event, helper) {
        var checked = component.get("v.ifCheckedCase")
        if (!checked) {
            helper.showOpenedCases(component, event, helper);
        } else if (checked) {

            helper.doinitMethod(component, event, helper);
        }

    },
    valueChanged: function (component, event, helper) {
        console.log(component.find("select"));
    }

})